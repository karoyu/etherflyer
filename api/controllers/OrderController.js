/**
 * OrderController
 *
 * @description :: Server-side logic for managing orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const request = require('request');
module.exports = {

		find: function(req, res) {
  

		if(req.query.type ==1)
		{
			var result = [{
    "result": {
        "limit": 50,
        "records": [
            {
                "mtime": 1509897026.812212,
                "market": "BTCUSD",
                "maker_fee": "0.001",
                "price": "10",
                "amount": "1",
                "id": 1,
                "type": 1,
                "side": 2,
                "user": 1,
                "left": "1",
                "ctime": 1509897026.812212,
                "taker_fee": "0.002",
                "deal_stock": "0e-8",
                "deal_money": "0e-16",
                "deal_fee": "0e-12"
            }
        ],
        "offset": 0,
        "total": 1
    },
    "error": null,
    "id": 52
}];

				res.json(result);


		} else
		{

  
			var result = [{
    "result": {
        "records": [
            {
                "market": "ETHUSD",
                "maker_fee": "0.001",
                "price": "22",
                "amount": "1",
                "id": 3,
                "ctime": 1509898392.194061,
                "side": 1,
                "user": 2,
                "taker_fee": "0.002",
                "ftime": 1509898392.194066,
                "source": "",
                "type": 1,
                "deal_stock": "1",
                "deal_money": "22",
                "deal_fee": "0.044"
            }
        ],
        "offset": 0,
        "limit": 50
    },
    "error": null,
    "id": 58
}];

		}  
                
			


		},

	Update: function(req, res) {
		var result = [{
		    "result": {
		        "source": "",
		        "mtime": 1509899160.8984621,
		        "market": "ZECUSD",
		        "maker_fee": "0.001",
		        "price": "30",
		        "amount": "0.5",
		        "id": 4,
		        "type": 1,
		        "user": 1,
		        "side": 2,
		        "left": "0.5",
		        "ctime": 1509899160.8984621,
		        "taker_fee": "0.002",
		        "deal_stock": "0",
		        "deal_money": "0",
		        "deal_fee": "0"
		    },
		    "error": null,
		    "id": 46
		}];

         res.json(result);

		}
	
};

