/**
 * BalanceController
 *
 * @description :: Server-side logic for managing balances
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

		findOne: function(req, res) { 
				var result = {
				"result": {
				    "BCC": {
				        "available": "0",
				        "freeze": "0"
				    },
				    "USD": {
				        "available": "10000",
				        "freeze": "0"
				    },
				    "ETH": {
				        "available": "0",
				        "freeze": "0"
				    },
				    "LTC": {
				        "available": "0",
				        "freeze": "0"
				    },
				    "BTC": {
				        "available": "1.5",
				        "freeze": "0"
				    },
				    "ZEC": {
				        "available": "0",
				        "freeze": "0"
				    },
				    "TCASH": {
				        "available": "0",
				        "freeze": "0"
				    }
				},
				"error": null,
				"id": 32
				};

				res.json(result);


		},
		Update: function(req, res) { 

			var result ={
			"result": {
			"status": "success"
			},
			"error": null,
			"id": 31
			}

         res.json(result);

		}
};

