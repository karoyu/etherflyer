/**
 * MarketController
 *
 * @description :: Server-side logic for managing markets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const request = require('request');

var scheduler = require('node-schedule');
var rule = new scheduler.RecurrenceRule();
rule.second = new scheduler.Range(0, 59, 10);

var resultkline86400 =[];
var resultkline300 =[];
var senddata  =[];

scheduler.scheduleJob(rule, function(){

      
     start = new Date().getTime() -60*60*1000+"";
     end = new Date().getTime()+"";

	 request('https://poloniex.com/public?command=returnTradeHistory&currencyPair=BTC_NXT&start='+start.substring(0,10)+'&end='+end.substring(0,10), function (error, response, body) {
                var data = JSON.parse(body);
                for(var i =0 ; i<data.length;i++)
                {
                	data[i].price = data[i].total /  data[i].amount; 
                }
                senddata  =[];
                senddata.push(data);
            }); 



////////////////////////////////////////////////////////////

		    var start86400 = new Date().setMonth(new Date().getMonth() - 30)+"";
            var req_str='https://poloniex.com/public?command=returnChartData&currencyPair=BTC_NXT&start='+start86400.substring(0,10)+'&end=9999999999&period='+86400;
			request(req_str, function (error, response, body) {    
				
				var data = JSON.parse(body);
				resultkline86400 =[];
　　　　　　　　　for(var i =0;i<data.length;i++)
	            {
	            	var tmp = [];
	            	tmp.push(data[i].date*1000);
                    tmp.push(data[i].open*10000);
                    tmp.push(data[i].close*10000);
                    tmp.push(data[i].high*10000);
                    tmp.push(data[i].low*10000);
                    tmp.push(data[i].volume);
                    tmp.push("BTC_NXT");

                     resultkline86400.push(tmp);
	            }
	        });

////////////////////////////////////////////////////////////

            var start300 = new Date().getTime()-6*60*60*1000+"";
       	

	         var req_str1='https://poloniex.com/public?command=returnChartData&currencyPair=BTC_NXT&start='+start300.substring(0,10)+'&end=9999999999&&period='+300;
			request(req_str1, function (error, response, body) {    
				//console.log(req_str1);
				var data = JSON.parse(body);
				resultkline300 =[];
　　　　　　　　　for(var i =0;i<data.length;i++)
	            {
	            	var tmp = [];
	            	tmp.push(data[i].date*1000);
                    tmp.push(data[i].open*10000);
                    tmp.push(data[i].close*10000);
                    tmp.push(data[i].high*10000);
                    tmp.push(data[i].low*10000);
                    tmp.push(data[i].volume);
                    tmp.push("BTC_NXT");

                     resultkline300.push(tmp);
	            }
	        });
	          
////////////////////////////////////////////////////////////

			
});




module.exports = {
	find: function(req, res) {  
		if(req.query.type =="status")
		{
			
			var result = [{
			    "result": {
        "open": "22",
        "period": 86400,
        "low": "22",
        "last": "22",
        "close": "22",
        "high": "22",
        "volume": "1"
    },
    "error": null,
    "id": 66
			}];

           res.json(result);

		}
		else if(req.query.type =="kline")
		{
			if(req.query.interval=="86400" || req.query.interval==86400)
	            res.json(resultkline86400);

	        else if(req.query.interval=="300" || req.query.interval==300)
	            res.json(resultkline300);

     

		}
		else if(req.query.type == "deals")
		{
           
            res.json(senddata);

		}

	}
	
};

