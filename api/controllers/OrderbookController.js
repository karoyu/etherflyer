/**
 * OrderbookController
 *
 * @description :: Server-side logic for managing orderbooks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const request = require('request');
var scheduler = require('node-schedule');
var rule = new scheduler.RecurrenceRule();
rule.second = new scheduler.Range(0, 59, 10);

var result;
scheduler.scheduleJob(rule, function(){

    request('https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_NXT&depth=16', function (error, response, body) {
       result=JSON.parse("["+body+"]");
    });    
      
});



module.exports = {

  find: function(req, res) {
  res.json(result);

  }
	
};

