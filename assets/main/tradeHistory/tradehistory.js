var tradeHistoryModule = angular.module('tradeHistoryModule', ['ngResource'
                                                        ,'ngSessionStorage']);


tradeHistoryModule.controller('tradeHistoryController', [
    '$scope',
    'market',
    '$interval',
    function(
        $scope,
        market,
        $interval
    ) {

        $scope.tradeHistory = [];

    toloadDeals();

    $scope.loadDeals = function(){

        toloadDeals();
    }

     $interval(function() {
        toloadDeals();
    }, 10*1000);

    function toloadDeals(){

     $scope.marketParams={};
     $scope.marketParams.market = 'BTCUSD';
     $scope.marketParams.limit =  '50';
     $scope.marketParams.last_id   =  "12323231213";
     $scope.marketParams.start = new Date().getTime() -60*60*1000;
     $scope.marketParams.end = new Date().getTime();
     $scope.marketParams.type   =  "deals";
     
    market.query($scope.marketParams).$promise.then(function(data) {
         console.log("tradedOrders",data);
         $scope.tradeHistory=[];
         angular.forEach(data[0],function(value,key){
          var tmp={};
          tmp.Date = value.date.substring(10);
          tmp.Price = value.price*10000;
          tmp.Amount = value.amount;
          if(value.type =='buy')
          {
            tmp.BuyOrSell = true;
          } else if(value.type == 'sell')
          {
            tmp.BuyOrSell = false;
          }
           $scope.tradeHistory.push(tmp);
         },null);

      }, function(err) {
          console.log(err);
      });
    }


    }
]);


tradeHistoryModule.factory('market', ['$resource',function ($resource) {
    return $resource('/market/:id',null,
    {
        'update': { method:'PUT' }
    });
}]);