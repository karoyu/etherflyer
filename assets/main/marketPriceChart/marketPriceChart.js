var marketModule = angular.module('marketModule', [
    'ngResource', 'ngSessionStorage'
]);

marketModule.controller('marketController', [
    '$scope', 'market','$interval',
    function(
        $scope, market,$interval
    ) {
////////////////////////////////////////
//init start                      //////
////////////////////////////////////////

        init();

        function init() {
            loadStatus();
            loadKline();
        };

        function loadStatus() {

            $scope.marketParams = {};
            $scope.marketParams.market = 'BTCUSD';
            $scope.marketParams.period = '86400';
            $scope.marketParams.type = "status";

            market.query($scope.marketParams).$promise.then(function(data) {
                console.log(data);
            }, function(err) {
                console.log(err);

            });
        };


        function loadKline() {

         loadKline8440(loadKline300);
        }

        $scope.data8440;
        $scope.data300;
        $scope.chart;

        function loadKline8440(callback){
            $scope.marketParams = {};
            $scope.marketParams.market = 'BTCUSD';
            $scope.marketParams.start = new Date().setMonth(new Date().getMonth() - 30);
            $scope.marketParams.end = new Date().getTime();
            $scope.marketParams.interval = "86400";
            $scope.marketParams.type = "kline";
            market.query($scope.marketParams).$promise.then(function(data8440) {
                  $scope.data8440 =data8440;
                    loadKline300(6*60*60*1000);
            }, function(err) {
                console.log(err);
            });

        }

        function loadKline300(offset){
            $scope.marketParams = {};
            $scope.marketParams.market = 'BTCUSD';
            $scope.marketParams.start = new Date().getTime() -offset;
            $scope.marketParams.end = new Date().getTime();
            $scope.marketParams.interval = "300";
            $scope.marketParams.type = "kline";
            market.query($scope.marketParams).$promise.then(function(data300) {
               var tmpdata8440 = angular.copy($scope.data8440);
                 $scope.data300 = data300;
               angular.forEach($scope.data300,function(value,key){
                    tmpdata8440.push(value);
               },null);
               loadDataToChart(tmpdata8440);

            }, function(err) {
                console.log(err);
            });
        }

function loadDataToChart(data){
       
         
        var ohlc = [],
        volume = [],
        dataLength = data.length,
       
        groupingUnits = [
            [
                'hour', // unit name
                [1, 2, 4, 6] // allowed multiples
            ],
            [
                'month', [1, 3, 5, 15, 30]
            ]
        ],

        i = 0;

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            data[i][0], // the date
            data[i][1], // open
            data[i][3], // high
            data[i][4], // low
            data[i][2] // close
        ]);

        volume.push([
            data[i][0], // the date
            data[i][5] // the volume
        ]);
    }
    // create the chart
    $scope.chart = Highcharts.stockChart('container', {
        chart: {
            backgroundColor: '#122028',
        },
        rangeSelector: {
            inputEnabled: false,
           
            selected: 1,
            buttons: [ {
                type: 'month',
                count: 5,
                text: '5m'
            }, {
                type: 'month',
                count: 15,
                text: '15m'
            }, {
                type: 'month',
                count: 30,
                text: '30m'
            },  {
                type: 'hour',
                count: 2,
                text: '2h'
            }, {
                type: 'hour',
                count: 4,
                text: '4h'
            }, {
                type: 'hour',
                count: 6,
                text: '6h'
            }]
        },

        xAxis: {
          events: {
            setExtremes: function(e) {
              if(typeof(e.rangeSelectorButton)!== 'undefined') {
                var c = e.rangeSelectorButton.count;
                var t = e.rangeSelectorButton.type;
                var btn_index = null;
                if(c == 1 && t == "hour"){
                
                } else if(c == 2 && t == "hour"){
                 
                } else if(c == 4 && t == "hour"){
                 
                } else if(c == 6 && t == "hour"){      
                } 
              }
            }
          }
        },
        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
           
            height: '60%',
            lineWidth: 2,
            resize: {
                enabled: true
            }
        }, {
            labels: {
                align: 'right',
                x: -3
            },
           
            top: '65%',
            height: '35%',
            offset: 0,
            lineWidth: 2
        }],

        tooltip: {
            split: true
        },

        plotOptions: {
            series: {
                dataLabels: {
                    color: '#B0B0B3'
                },
                marker: {
                    lineColor: '#333'
                }
            },
            candlestick: {
                color: '#0f8dbc',
                upColor: 'red',
                lineColor: '#0f8dbc',
                upLineColor: 'red',
                maker: {
                    states: {
                        hover: {
                            enabled: false,
                        }
                    }
                }
            },
            boxplot: {
                fillColor: '#505053'
            },
            errorbar: {
                color: 'white'
            }
        },

        series: [{
            type: 'candlestick',
            name: 'OHLC',
            data: ohlc,
            dataGrouping: {
                units: groupingUnits
            }
        }, {
            type: 'column',
            name: 'Amount',
            data: volume,
            yAxis: 1,
            dataGrouping: {
                units: groupingUnits
            }
        }]
    });

}
////////////////////////////////////////
//init end                        //////
////////////////////////////////////////

///////////////////////////
//update chart line      //
///////////////////////////

     $interval(function() {
         if($scope.chart !=null && $scope.chart !=undefined)
         {
            $scope.marketParams = {};
            $scope.marketParams.market = 'BTCUSD';
            $scope.marketParams.start = new Date().getTime() -8*60*1000;
            $scope.marketParams.end = new Date().getTime();
            $scope.marketParams.interval = "300";
            $scope.marketParams.type = "kline";
            market.query($scope.marketParams).$promise.then(function(data) {

              
                var addData=[];
               
               var value = data[data.length-1];  
                  if(value[0]!=0 && value[0]!='0')
                  { 
                        if(checkData300(value)==false)
                        {
                            $scope.data300.push(value);
                            addData.push(value);
                        }else{
                            var refreshData=[];
                            refreshData.push(value);
                            refreshChartLine(refreshData,'update');
                        }
                  }
               
              

               if(addData.length>0 )
               {
                refreshChartLine(addData,'add');
               }
               

            }, function(err) {
                console.log(err);
            });
         }
    }, 10*1000);


function  checkData300(data){
    var ret = false;
    $scope.data300
    angular.forEach($scope.data300,function(value,key){
      if(value[0] == data[0])
      {
         ret = true;
      }

    },null);
    return ret;

}     


function refreshChartLine(data,flag){
var ohlc = [],
volume = [],
dataLength = data.length,

groupingUnits = [
[
'hour', // unit name
[1, 2, 4, 6] // allowed multiples
],
[
'month', [1, 3, 5, 15, 30]
]
],

i = 0;

for (i; i < dataLength; i += 1) {
ohlc.push([
data[i][0], // the date
data[i][1], // open
data[i][3], // high
data[i][4], // low
data[i][2] // close
]);

volume.push([
data[i][0], // the date
data[i][5] // the volume
]);
}

if(flag =='add')
{
 angular.forEach(ohlc,function(value,key){
    if(value[0]!=0 && value[0]!='0')
     $scope.chart.series[0].addPoint(value);
 },null);

 angular.forEach(volume,function(value,key){
    if(value[0]!=0 && value[0]!='0')
     $scope.chart.series[1].addPoint(value);
 },null);
   
}else if(flag =='update')
{
    var ohlcdata =$scope.chart.series[0].data;
    if(ohlcdata[ohlcdata.length-1])
    ohlcdata[ohlcdata.length-1].update(ohlc[0]);
    var volumedata =$scope.chart.series[1].data;
    if(volumedata[volumedata.length-1])
    volumedata[volumedata.length-1].update(volume[0]);
}

}



///////////////////////////
//update chart line      //
///////////////////////////

    }
]);

marketModule.factory('market', ['$resource', function($resource) {
    return $resource('/market/:id', null, {
        'update': { method: 'PUT' }
    });
}]);





function afterSetExtremes(e) {

    var chart = Highcharts.charts[0];

    chart.showLoading('Loading data from server...');
    $.getJSON('https://www.highcharts.com/samples/data/from-sql.php?start=' + Math.round(e.min) +
        '&end=' + Math.round(e.max) + '&callback=?',
        function(data) {

            chart.series[0].setData(data);
            //chart.hideLoading();
        });
}

