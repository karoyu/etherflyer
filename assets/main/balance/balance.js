var balanceModule = angular.module('balanceModule', [ 
                                                        'ngResource'
                                                        ,'ngSessionStorage'
                                                    ]);

balanceModule.controller('balanceController',
                                                [
                                                  '$scope'
                                                 ,'balance'  
                                                 ,'uuid2'
                                                 ,function( 
                                                   $scope
                                                 , balance
                                                 , uuid2 
                                                  ) {
   
    $scope.balace=$scope.user.balance;
    $scope.balace.buyTCash={};
    $scope.balace.buyTCash.amout;
    $scope.balace.buyTCash.price;
    $scope.balace.buyTCash.youpay;
    $scope.balace.buyTCash.type =1;
    $scope.balace.sellTCash={};
    $scope.balace.sellTCash.amout="";
    $scope.balace.sellTCash.price ="";
    $scope.balace.sellTCash.youown ="";
    $scope.balace.sellTCash.type =2;
    $scope.balace.dateTimeNow = new Date();

    
    init();

    function init(){
       loadBalance();
    };

    function loadBalance(){
      balance.get({id:$scope.user.userid}).$promise.then(function(data) {
        $scope.balace.tcash =data.result.TCASH.available;
        $scope.balace.eth =data.result.ETH.available;
      }, function(err) {
        console.log(err);
      
      });
    }

    
    $scope.calculateYouPay=function(keyEvent)
   {
    
         $scope.balace.buyTCash.youpay = $scope.balace.buyTCash.amout * $scope.balace.buyTCash.price;
     
    }

    $scope.buy=function(marketOrLimit){

     bootbox.confirm("Are you sure to execute this transaction?", 
      function(result)
      { 
           if(result)
           {
               buy(marketOrLimit);
           }
      });

     
    };

    function buy(marketOrLimit){
      $scope.balace.buyTCash.marketOrLimit=marketOrLimit;
      $scope.user.openOrders.push( { Date: new Date(), 
        Type: 1,
         Price: $scope.balace.buyTCash.price, 
         PriceUp: true, 
         Amount: $scope.balace.buyTCash.amout, 
         Total: $scope.balace.buyTCash.youpay, 
         Cancel: false, 
         orderid: uuid2.newuuid()});

      $scope.calculateOpenOrders();
     
      balance.update({id:$scope.user.userid},$scope.balace.buyTCash).$promise.then(function(data) {
       
      }, function(err) {
        console.log(err);
      });


    }



    $scope.sell=function(marketOrLimit){
      bootbox.confirm("Are you sure to execute this transaction?", 
      function(result)
      { 
           if(result)
           {
               sell(marketOrLimit);
           }
      });
    }

    function sell(marketOrLimit){
      $scope.balace.sellTCash.marketOrLimit=marketOrLimit;
      $scope.user.openOrders.push
       ( 
        {
         Date: new Date(), 
         Type: 2,
         Price: $scope.balace.buyTCash.price, 
         PriceUp: false, 
         Amount: $scope.balace.buyTCash.amout, 
         Total: $scope.balace.buyTCash.youpay, 
         Cancel: false, 
         orderid: uuid2.newuuid() 
         }
       );

      $scope.calculateOpenOrders();
       
      balance.update({id:$scope.user.userid},$scope.balace.sellTCash).$promise.then(function(data) {
       
      }, function(err) {
        console.log(err);
      });

    }


  


}]);
balanceModule.directive('swithBalance', function() {
    return {
        templateUrl: '../main/balance/balanceSwitch.html'
    };
});

balanceModule.factory('balance', ['$resource',function ($resource) {
    return $resource('/balance/:id',null,
    {
        'update': { method:'PUT' }
    });
}]);
