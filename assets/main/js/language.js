var lang_en = {
	charts:'Charts',
	trade:'Trade',
	open_orders:'Open Orders',
	price:'Price',
	amount:'Amount',
	sell:'SELL',
	buy:'BUY',
	candle:'Candle',
	depth:'Depth',
	buy_tcash: 'Buy TCASH',
	sell_tcash:'Sell TCASH',
	you_pay:'You Pay',
	date:'Date',
	type:'Type',
	cancel:'Cancel',
	total:'Total',
	my_history:'My History'
};
var lang_ja = {
	charts:'Charts',
	trade:'Trade',
	open_orders:'Open Orders',
	price:'Price',
	amount:'Amount',
	sell:'SELL',
	buy:'BUY',
	candle:'Candle',
	depth:'Depth',
	buy_tcash: 'Buy TCASH',
	sell_tcash:'Sell TCASH',
	you_pay:'You Pay',
	date:'Date',
	type:'Type',
	cancel:'Cancel',
	total:'Total',
	my_history:'My History'
};
var lang_ko = {
	charts:'Charts',
	trade:'Trade',
	open_orders:'Open Orders',
	price:'Price',
	amount:'Amount',
	sell:'SELL',
	buy:'BUY',
	candle:'Candle',
	depth:'Depth',
	buy_tcash: 'Buy TCASH',
	sell_tcash:'Sell TCASH',
	you_pay:'You Pay',
	date:'Date',
	type:'Type',
	cancel:'Cancel',
	total:'Total',
	my_history:'My History'
};
var lang_ch = {
	charts:'Charts',
	trade:'交易',
	open_orders:'进行中订单',
	price:'价格',
	amount:'数量',
	sell:'卖出',
	buy:'买入',
	candle:'Candle',
	depth:'Depth',
	buy_tcash: '买 TCASH',
	sell_tcash:'卖 TCASH',
	you_pay:'支付',
	date:'日期',
	type:'类型',
	cancel:'取消',
	total:'总计',
	my_history:'历史记录'
}
	
$(".language").change(function(){
	var val  = $(this).val();
	$(".change_lang").each(function(){
		
		if(val == 1){
			$(this).text(lang_en[$(this).attr("lan")]);
		}
		if(val == 2){
			$(this).text(lang_ja[$(this).attr("lan")]);
		}
		if(val == 3){
			$(this).text(lang_ko[$(this).attr("lan")]);
		}
		if(val == 4){
			
			$(this).text(lang_ch[$(this).attr("lan")]);
		}
	});
	
});