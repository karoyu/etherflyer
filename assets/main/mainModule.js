var mainModule = angular.module('mainModule', [
    'ngResource', 'angularUUID2', 'ngSessionStorage', 'tradeHistoryModule', 'balanceModule', 'openOrdersModule', 'orderBookModule', 'tradedOrdersModule', 'headerModule', 'marketModule', 'signupModule'

]);

mainModule.controller('mainController', [
    '$scope',
    function(
        $scope
    ) {
        $scope.user = {};
        $scope.user.userid = "eric_dwj";
        $scope.user.header = {};
        $scope.user.balance = {};
        $scope.user.openOrders = [];
        $scope.user.balance.showBuyOrSell = false; //true  show buy  false show sell
    
        $scope.user.openOrdersCnt=0;
        $scope.chart_dept=true;
       
        $scope.swith={};
        $scope.swith.chart_order_trade='chart';
        $scope.swith.openorder_history=true;
        $scope.swith.balancd_history=true;
        
        $scope.calculateOpenOrders=function(){
         var cnt =0;

         angular.forEach($scope.user.openOrders,function(value,key){
            if(value.Cancel==false)
            {
                cnt ++;
            }

         },null);

         $scope.user.openOrdersCnt = cnt;

        }

  

    }
]);


mainModule.directive('marketPriceChart', function() {
    return {
        templateUrl: '../main/marketPriceChart/marketPriceChart.html'
    };
});
mainModule.directive('tradeHistory', function() {
    return {
        templateUrl: '../main/tradeHistory/tradeHistory.html'
    };
});

mainModule.directive('balance', function() {
    return {
        templateUrl: '../main/balance/balance.html'
    };
});


mainModule.directive('openOrders', function() {
    return {
        templateUrl: '../main/openOrders/openOrders.html'
    };
});

mainModule.directive('tradedOrders', function() {
    return {
        templateUrl: '../main/tradedOrders/tradedOrders.html'
    };
});

mainModule.directive('orderBook', function() {
    return {
        templateUrl: '../main/orderBook/orderBook.html'
    };
});

mainModule.directive('exchageHeader', function() {
    return {
        templateUrl: '../main/header/header.html'
    };
});

mainModule.directive('buySellBtn', function() {
    return {
        templateUrl: '../main/buysellbtn.html'
    };
});
mainModule.directive('switchOpenorderHistory', function() {
    return {
        templateUrl: '../main/switchOpenOrdersHistory.html'
    };
});
