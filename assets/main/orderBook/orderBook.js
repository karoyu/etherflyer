var orderBookModule = angular.module('orderBookModule', [
                                                          'ngResource'
                                                        ,'ngSessionStorage'
                                                        ]);

orderBookModule.controller('orderBookController',
                                                [
                                                  '$scope'
                                                 ,'orderBook'
                                                 ,'$interval'  
                                                 ,function( 
                                                  $scope
                                                  ,orderBook
                                                  ,$interval
                                                  ) {

    $scope.orderBooks =[
    // {TCashPrice:298.0,TCashAmount:1.9888,TCashTotal:20,ETHPrice:298.0,ETHAmount:1.9888,ETHTotal:20},
    // {TCashPrice:100.0,TCashAmount:2.5888,TCashTotal:20,ETHPrice:298.0,ETHAmount:1.9888,ETHTotal:20},
    // {TCashPrice:398.0,TCashAmount:4.5866,TCashTotal:20,ETHPrice:298.0,ETHAmount:1.9888,ETHTotal:20}
           ];

     $scope.odrerBookParams={};
     $scope.odrerBookParams.market = 'BTCUSD';
     $scope.odrerBookParams.side = 1;
     $scope.odrerBookParams.offset =0;
     $scope.odrerBookParams.limit =50;
    

   init();

    function init(){
       loadOrderBook();
    };

     $interval(function() {
            loadOrderBook();
        }, 10*1000);

    function loadOrderBook(){
          
      orderBook.query($scope.odrerBookParams).$promise.then(function(data) {
        var orderBooks =[];
         angular.forEach(data[0].asks,function(value,key){
             var tmp ={};
             
             tmp.TCashPrice = value[0]*10000;
             tmp.TCashAmount = value[1];
             tmp.TCashTotal =tmp.TCashAmount * tmp.TCashPrice;
             orderBooks.push(tmp);
         },null);

         for(var i=0; i< orderBooks.length;i++)
         {
            orderBooks[i].ETHPrice  = data[0].bids[i][0]*10000;
            orderBooks[i].ETHAmount  = data[0].bids[i][1];
            orderBooks[i].ETHTotal =data[0].bids[i][0] *  data[0].bids[i][1]*10000;
         }
         $scope.orderBooks =orderBooks;
      }, function(err) {
         console.log(err);
      });
    }


  


}]);

orderBookModule.factory('orderBook', ['$resource',function ($resource) {
    return $resource('/orderBook/:id',null,
    {
        'update': { method:'PUT' }
    });
}]);
