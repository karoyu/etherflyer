var openOrdersModule = angular.module('openOrdersModule', [
    'ngResource', 'ngSessionStorage'
]);

openOrdersModule.controller('openOrdersController', [
    '$scope', 'order', '$interval',
    function(
        $scope, order,$interval
    ) {

        $scope.openOrders = [
            // { Date: new Date(), Type: 1, Price: 298.0, PriceUp: true, Amount: 1.9888, Total: 20, Cancel: true, orderid: 'abcd' },
            // { Date: new Date(), Type: 2, Price: 100.0, PriceUp: false, Amount: 2.5888, Total: 20, Cancel: false, orderid: 'abcd' },
            // { Date: new Date(), Type: 1, Price: 398.0, PriceUp: true, Amount: 4.5866, Total: 20, Cancel: true, orderid: 'abcd' }
        ];

        $scope.user.openOrders = $scope.openOrders;

        init();

        function init() {
            loadPendingOrder();
        };

        $scope.loadPendingOrder = function() {
            loadPendingOrder();

        }

       

        function loadPendingOrder() {

            $scope.PendingodrerParams = {};
            $scope.PendingodrerParams.user_id = $scope.user.userid;
            $scope.PendingodrerParams.market = 'ZECUSD';
            $scope.PendingodrerParams.offset = 0;
            $scope.PendingodrerParams.limit = 500000;
            $scope.PendingodrerParams.type = 1;


            order.query($scope.PendingodrerParams).$promise.then(function(data) {
                console.log(data);
            }, function(err) {
                console.log(err);

            });
        }






        $scope.cancel = function(orderid) {


            angular.forEach($scope.user.openOrders,function(value,key){
              if(value.orderid == orderid)
              {
                value.Cancel=true;
              }

            },null);


            $scope.calculateOpenOrders();

            $scope.odrerParams = {};
            $scope.odrerParams.user_id = $scope.user.userid;
            $scope.odrerParams.market = 'ZECUSD';
            $scope.odrerParams.order_id = orderid;

            order.update({ id: orderid }, $scope.odrerParams).$promise.then(function(data) {

            }, function(err) {});


        }



    }
]);



openOrdersModule.factory('order', ['$resource', function($resource) {
    return $resource('/order/:id', null, {
        'update': { method: 'PUT' }
    });
}]);