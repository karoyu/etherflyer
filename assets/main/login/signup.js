var signupModule = angular.module('signupModule', [
    'ngResource', 'ngSessionStorage'
]);

signupModule.controller('signupController', ['$scope',
    function(
        $scope
    ) {
        $scope.showSignupBtn = true;
        $scope.onSignupClick = function() {
            $scope.showLabel = true;
            $scope.showSigninBtn = true;
            $scope.showSignupBtn = false;
        }
    }
]);