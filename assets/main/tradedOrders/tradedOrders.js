var tradedOrdersModule = angular.module('tradedOrdersModule', ['ngResource'
                                                        ,'ngSessionStorage']);

tradedOrdersModule.controller('tradedOrdersController',
                                                [
                                                   '$scope'
                                                    ,'order'
                                                 ,function( 
                                                  $scope
                                                 ,order
                                                  ) {

     $scope.tradedOrders =[];
    
     $scope.loadTradedOrder = function(){
      loadTradedOrder();

     }

     function loadTradedOrder(){

        $scope.TradedodrerParams={};
     $scope.TradedodrerParams.user_id =  $scope.user.userid;
     $scope.TradedodrerParams.market = 'ZECUSD';
     $scope.TradedodrerParams.offset =0;
     $scope.TradedodrerParams.limit =500000;
     $scope.TradedodrerParams.side  =1;
     $scope.TradedodrerParams.type  =2;


      order.query($scope.TradedodrerParams).$promise.then(function(data) {
       console.log(data);
      }, function(err) {
      console.log(err);
      });
    }


}]);
tradedOrdersModule.factory('order', ['$resource',function ($resource) {
    return $resource('/order/:id',null,
    {
        'update': { method:'PUT' }
    });
}]);