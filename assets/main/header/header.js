var headerModule = angular.module('headerModule', [
    'ngResource'
   ,'ngSessionStorage'
  ]);

headerModule.controller('headerController', [
'$scope',
'header',
'$interval',
function(
$scope,
header,
$interval
) {

$scope.header = $scope.user.header;
$scope.header.pairs = []
$scope.header.pairs.push("TCASH/ETH");
$scope.header.ethAddress = "0x0123456789012345678901234567890123456789";
$scope.header.ethAddressShow = $scope.header.ethAddress.substring(0, 5) + "...";
$scope.header.language = [];
$scope.header.language.push("English");

loadHeaderData();

function loadHeaderData(){

$scope.header.headParams={};
$scope.header.headParams.market ="BTC_NXT"
header.query($scope.header.headParams).$promise.then(function(data) {
console.log(data[0]);
$scope.header.changeRate=data[0].percentChange*100;
$scope.header.tradePrice=data[0].last*10000;
}, function(err) {
console.log(err);
});
}

$scope.changeTheme = function() {
$scope.showLabel = true;
$scope.showSigninBtn = true;
$scope.showSignupBtn = false;
}

$interval(function() {
loadHeaderData();
}, 10*1000);

$scope.chartOrderTradeChange=function(str){
$scope.swith.chart_order_trade=str;
}


}
]);
headerModule.factory('header', ['$resource',function ($resource) {
return $resource('/header/:id',null,
{
'update': { method:'PUT' }
});
}]);

headerModule.directive('swith', function() {
    return {
        templateUrl: '../main/header/swithChartsTradeOpenOrder.html'
    };
});